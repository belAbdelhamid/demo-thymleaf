package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {
   @RequestMapping(value = "/index")
   public String index() {
      return "index";
   }
   @RequestMapping(value = "/view-product")
   public String Viewproduct() {
      return "view-product";
   }
   @RequestMapping(value = "/add-product")
   public String addproduct() {
      return "add-product";
   }
   @RequestMapping("/locale")
   public String locale() {
      return "locale";
   }
}